=========
Changelog
=========

0.1 - 31 January 2013
~~~~~~~~~~~~~~~~~~~~~

* First integration with mailup service.

0.1.1 - 31 January 2013
~~~~~~~~~~~~~~~~~~~~~~~

* Add dist and egg-info directory in .hgignore

0.1.2 - 31 January 2013
~~~~~~~~~~~~~~~~~~~~~~~

* Add doc in checkSubscriber function